# cross-application-authenticator

This plugin allows you to transfer a user session from one application to
another e.g. open a website from an app and automatically log the user in.

## Installation

* `composer require e3creative/cross-application-authenticator`
* `php artisan vendor:publish`

## Routes.

This package adds two routes to your application. They can be customised in this
package's config, but by default will be:

* POST /api/v1/cross-application-authenticator
    Middleware: auth:api
    Required headers:
        Accept: application/json
        Content-type: application/json
    Required data:
        url: the URL that the user should be authorised to be transferred to
    Returns: the newly created token (object)

* GET /cross-application-authenticator []
    Middleware: web
    Required data:
        token: the token returned by the POST request


The intial POST request relies on the user being available to the request, so
you should authenticate the user as you
normally would for your request (e.g. by passing an access token).


## Config

The following configuration will be available to you after publishing the vendor
files:

route_name:
    Description: theroute fragmant that will be used to create your endpoint
        routes
    Default: 'cross-application-authenticator'

route_middleware_name:
    Description: the middleware that should be applied to your POST endpoint to
        ensure that only authorized users are able to retrieve a token
    Default: 'auth:api'

route_prefix:
    Description: the route prefix for your POST endpoint
    Default: 'api/v1'

failed_authentication_redirect:
    Description: the route name or URL that the GET endpoint should redirect to
        if the token supplied is missing or invalid
    Default: 'login'

token_lifetime:
    Description: how long in SECONDS the transfer token should be valid for
        after generating it
    Default: 60

