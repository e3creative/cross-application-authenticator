# Changelog

## [2.0.0] - 2019-10-31
### Changed
- Updates vendor name

## [1.0.2] - 2018-09-19
### Fixes:
- Version compatibility with Laravel 5.6

## [1.0.1] - 2018-09-18
### Fixes:
- Config file loading

## [1.0.0] - 2018-09-18
### Initial release
