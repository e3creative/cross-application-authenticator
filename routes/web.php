<?php

$router = Route::group(['middleware' => 'web'], function ($router) {
    $router->get(
        config('cross-application-authenticator.route_name'),
        'E3Creative\CrossApplicationAuthenticator\Controllers\CrossSiteAuthenticatorController@index'
    );
});
