<?php

$parameters = [
    'middleware' => config('cross-application-authenticator.route_middleware_name'),
    'prefix' => config('cross-application-authenticator.route_prefix'),
];

$router = Route::group(array_filter($parameters), function ($router) {
    $router->post(
        config('cross-application-authenticator.route_name'),
        'E3Creative\CrossApplicationAuthenticator\Controllers\CrossSiteAuthenticatorController@store'
    );
});
