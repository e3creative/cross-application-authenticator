<?php

return [
    /**
     * Route Name:
     *
     * The route fragment that should be used to build your CAA token routes, e.g.
     *     GET: /[FRAGMENT]
     *     POST: /api/v1/[FRAGMENT]
     */
    'route_name' => 'cross-application-authenticator',

    /**
     * Route Middleware Name:
     *
     * The middleware that should be applied to the token retrieval POST route
     * to ensure that only authorised users are able to retrieve a token (string
     * or array is fine)
     */
    'route_middleware_name' => 'auth:api',

    /**
     * Route Prefix:
     *
     * A prefix to apply to the token retrieval POST route
     */
    'route_prefix' => 'api/v1',

    /**
     * Failed Authentication Redirect:
     *
     * Where to redirect the user to in the transfer GET request. Route name or
     * path is fine, route name will be tried first.
     */
    'failed_authentication_redirect' => 'login',

    /**
     * Token Lifetime:
     *
     * The length of time in seconds that the token should be valid for
     */
    'token_lifetime' => 60,
];
