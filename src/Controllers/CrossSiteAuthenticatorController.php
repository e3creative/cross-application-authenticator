<?php

namespace E3Creative\CrossApplicationAuthenticator\Controllers;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Router;
use E3Creative\CrossApplicationAuthenticator\Models\CrossApplicationAuthenticationToken;

class CrossSiteAuthenticatorController extends Controller
{
    /**
     * @var AuthManager
     */
    public $auth;

    /**
     * @var Router
     */
    public $router;

    /**
     * @var CrossApplicationAuthenticationToken
     */
    public $model;

    /**
     * @param AuthManager                         $auth
     * @param Router                              $router
     * @param CrossApplicationAuthenticationToken $model
     */
    public function __construct(AuthManager $auth, Router $router, CrossApplicationAuthenticationToken $model)
    {
        $this->auth = $auth;
        $this->router = $router;
        $this->model = $model;
    }

    /**
     * Finds a valid token, logs in the corresponding user, invalidates the
     * token for future use and redirects to the intended URL. If no valid token
     * can be found, redirect them to the login page
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $token = $this->model->valid($request->input('token'))->first();

        if (!$token) {
            $redirect = config('cross-application-authenticator.failed_authentication_redirect');

            return $this->router->has($redirect)
                ? redirect()->route($redirect)
                : redirect()->to($redirect);
        }

        $this->auth->loginUsingId($token->user_id);

        $token->update(['used' => 1]);

        return redirect()->to($token->url);
    }

    /**
     * Generates a cross authentication token object and returns it in the
     * response
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['url' => 'required|url']);

        $token = $this->model->create([
            'user_id' => $request->user()->id,
            'token'   => Uuid::uuid4()->toString(),
            'url'     => $request->input('url'),
        ]);

        return response()->json($token);
    }
}
