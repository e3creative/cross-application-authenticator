<?php

namespace E3Creative\CrossApplicationAuthenticator\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CrossApplicationAuthenticationToken extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param  Builder $query
     * @param  string  $token
     * @return Builder
     */
    public function scopeValid(Builder $query, string $token)
    {
        $validPeriod = Carbon::now()->subSeconds(
            config('cross-application-authenticator.token_lifetime')
        );

        return $query
            ->whereUsed(false)
            ->whereToken($token)
            ->where('created_at', '>=', $validPeriod);
    }
}
